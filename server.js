const express = require('express');
const path = require('path');

const app = express();

// run app by serving static files in dist directory
app.use(express.static(__dirname + '/dist'));

//handles sub routes
app.get('/*', (req, res) =>{
    res.sendFile(path.join(__dirname + '/dist/index.html'));
});

// start app by listening to default port
app.listen(process.env.PORT || 8080);