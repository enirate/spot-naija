import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegionService } from '../../admin-services/region.service';
import { FactsheetsService } from '../../admin-services/factsheets.service';


@Component({
  selector: 'app-factsheets',
  templateUrl: './factsheets.component.html',
  styleUrls: ['./factsheets.component.css', '../../../assets/css/sb-admin.css']
})
export class FactsheetsComponent implements OnInit {

  factForm:FormGroup;
  myRegions;
  allFactsheet:any;
  cantAdd:boolean = false;
  allMyFactsheet:any;

  constructor(
    private fb:FormBuilder, 
    private regionService:RegionService,
    private factsheetsService: FactsheetsService
    ) { }

  ngOnInit() {
    this.factForm = this.fb.group({
      title: [null, Validators.required],
      region: [null, Validators.required],
      details: [null, Validators.required]
    });

    this.regionService.getAllRegions()
    .subscribe(regions => {
      this.myRegions = regions.data.regions_details;
      console.log(this.myRegions);
  });

    this.factsheetsService.getAllFactsheet()
    .subscribe(res => {
      console.log(res.data.fact_sheet_details);
      this.allMyFactsheet = res.data.fact_sheet_details;
    });
  }

  addFactsheet(value){
    console.log(value);
    this.factsheetsService.addAFactsheet(value)
    .subscribe(res => {
      console.log(res)
      if(res.status === 'success')
      {
        location.reload(true)
      }
      else
      {
         this.cantAdd = true;
      }
    });
  }



}
