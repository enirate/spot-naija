import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { UsersService } from '../../admin-services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css', '../../../assets/css/sb-admin.css']
})
export class UsersComponent implements OnInit {

  userForm:FormGroup;
  editCopy:any;
  userDetails:any;

  constructor(private fb:FormBuilder, private usersService: UsersService) { }

  ngOnInit() {

    // fetch user
    this.populateTable();

    this.userForm = this.fb.group({
      name: [null, Validators.required],
      email: [null, Validators.compose([Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)])],
      phone: [null, Validators.compose([Validators.required, Validators.pattern(/\d{11}/i)])],
      password: [null, Validators.compose([Validators.required, Validators.pattern(/.{8,}/i)])],
      location: [null, Validators.required]
    })
  }

  populateTable(){
    // fetch user
    this.usersService.getUsers()
    .subscribe(res => {
      this.userDetails = res.data.data;
      // console.log(this.userDetails);      
    });
  }

  addUser(value){
    value.request = 'new';
    this.usersService.addUser(value)
    .subscribe(res => {
      // console.log(res);
      location.reload(true);
    });
    // console.log(value);
  }

// blocck user
  blockUser(id){
    console.log(id);
    if(confirm('Block User?')){
       this.usersService.blockUser(id)
      .subscribe(res => {
        // console.log(res);
        this.populateTable();
      });
    }
  }

  // activate edit
  activateEdit(userDetails){
    this.editCopy = userDetails;
  }

  // update user details
  updateUser(userDetails){
    userDetails.request = "update";
    // console.log(userDetails);
    this.usersService.updateUser(userDetails)
    .subscribe(res => {
      // console.log(res);
      location.reload(true);
    });
  }

}
