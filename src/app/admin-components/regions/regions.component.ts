import { Component, OnInit } from '@angular/core';
import { RegionService } from '../../admin-services/region.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

@Component({
  selector: 'app-regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.css', '../../../assets/css/sb-admin.css']
})
export class RegionsComponent implements OnInit {
  regionForm:FormGroup;
  successNote:boolean = false;
  regionList:any;

  constructor(private regionService:RegionService, private fb:FormBuilder) { }

  ngOnInit() {

    // validation
    this.regionForm = this.fb.group({
      region_name: [null, Validators.required]
    });

    this.regionService.getAllRegions()
    .subscribe(res => {
      console.log(res.data.regions_details);
      console.log(res);
      this.regionList = res.data.regions_details;
    });
  }

  addRegion(value){
    this.regionService.addRegion(value)
    .subscribe(res => {
      console.log(res.status);
      if (res.status === "success") 
      {
        this.successNote = true;
        location.reload(true);
      }
      else
      {
        if(prompt("Location already exist"))
        {
          location.reload(true);
        }
      }
    });
    // console.log(value);
  }

}
