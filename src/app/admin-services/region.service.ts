import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import'rxjs/add/operator/map'

@Injectable()
export class RegionService {

  baseUrl:string = 'http://projaro.com/app/spotnaija/api/v1/';

  constructor(private _http:Http) { }

  addRegion(value){
    var body = JSON.stringify(value);
    var header = new Headers({'Content-Type':'application/json'});
    var options = new RequestOptions({headers:header});

    return this._http.post(`${this.baseUrl}addregion`, body, options)
    .map(res => res.json());
  }

  getAllRegions(){
    return this._http.get(`${this.baseUrl}getregions`)
    .map(res => res.json());
  }

}
