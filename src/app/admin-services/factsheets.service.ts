import { Injectable } from '@angular/core';
import { RequestOptions, Headers, Http } from '@angular/http';

@Injectable()
export class FactsheetsService {

  baseUrl:string='http://projaro.com/app/spotnaija/api/v1/';

  constructor(private _http:Http) { }

  addAFactsheet(value){
    let body = JSON.stringify(value);
    let header = new Headers({'Content-Type':'text/json'});
    let options = new RequestOptions({headers:header});
    return this._http.post(`${this.baseUrl}addfactsheet`, body, options)
    .map(res => res.json());
  }

  getAllFactsheet(){
    return this._http.get(`${this.baseUrl}getfactsheet`)
    .map(res => res.json());
  }

}
