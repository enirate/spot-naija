import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class UsersService {
  baseUrl:string = 'http://projaro.com/app/spotnaija/api/v1/';

  // request options
  headers = new Headers({'Content-Type':'application/json'});
  options = new RequestOptions({headers:this.headers});

  constructor(private http:Http) { }

  // add new user
  addUser(details){
    let body = JSON.stringify(details);
    return this.http.post(`${this.baseUrl}douser`, body, this.options)
    .map((response) => response.json())
  }

  // update user info
  updateUser(details){
    let body = JSON.stringify(details);
    return this.http.post(`${this.baseUrl}douser`, body, this.options)
    .map((response) => response.json());
  }

  // block user
  blockUser(id){
    return this.http.get(`${this.baseUrl}user/block/${id}`)
    .map((response) => response.json());
  } //user/get

  getUsers(){
    return this.http.get(`${this.baseUrl}user/get`)
    .map((response) => response.json());
  }
  

  // delete User
  deleteUser(details){
    let body = JSON.stringify(details);
    return this.http.post(`${this.baseUrl}user/delete`, body, this.options)
    .map((response) => response.json());
  }

}
