/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FactsheetsService } from './factsheets.service';

describe('FactsheetsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FactsheetsService]
    });
  });

  it('should ...', inject([FactsheetsService], (service: FactsheetsService) => {
    expect(service).toBeTruthy();
  }));
});
