import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// components
import { LeaderboardComponent } from './admin-components/leaderboard/leaderboard.component';
import { UsersComponent } from './admin-components/users/users.component';
import { AdminComponent } from './admin-components/admin/admin.component';
import { LoginComponent } from './admin-components/login/login.component';
import { DashboardComponent } from './admin-components/dashboard/dashboard.component';
import { FactsheetsComponent } from './admin-components/factsheets/factsheets.component';
import { QuestionsComponent } from './admin-components/questions/questions.component';
import { ErrorComponent } from './admin-components/error/error.component';
import { RegionsComponent } from './admin-components/regions/regions.component';
import { HomeComponent } from './users-components/home/home.component';

const AppRoutes:Routes = [
    {
        path:'admin',
        component: AdminComponent,
        children:[
            {
                path:'login',
                component: LoginComponent
            },
            {
                path:'dashboard',
                component: DashboardComponent,
                children:[
                    {
                        path:'factsheets',
                        component: FactsheetsComponent
                    },
                    {
                        path:'questions',
                        component: QuestionsComponent
                    },
                    {
                        path:'regions',
                        component: RegionsComponent
                    },
                    {
                        path:'users',
                        component: UsersComponent,
                    },
                    {
                        path:'leaderboard',
                        component: LeaderboardComponent,
                    }
                    // {
                    //     path:'',
                    //     pathMatch:'full',
                    //     redirectTo:'factsheets'
                    // },
                    // {
                    //     path:'**',
                    //     component: ErrorComponent
                    // }
                ]
            },
            {
                path:'',
                pathMatch:'full',
                redirectTo:'login'
            },
            {
                path:'**',
                component: ErrorComponent
            }
        ]
    },
    {
        path:'**',
        component: HomeComponent
    }
]

@NgModule({
    imports:[RouterModule.forRoot(AppRoutes)],
    exports:[RouterModule]
})

export class RouteModule {}

export const RoutedComponents = [
    AdminComponent,
    UsersComponent,
    LeaderboardComponent,
    LoginComponent,
    ErrorComponent,
    HomeComponent,
    RegionsComponent,
    DashboardComponent,
    FactsheetsComponent,
    QuestionsComponent
] 