import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { RegionService } from './admin-services/region.service';
import { UsersService } from './admin-services/users.service';
import { FactsheetsService } from './admin-services/factsheets.service';
import { RouteModule, RoutedComponents } from './app.routing';

@NgModule({
  declarations: [
    AppComponent,
    RoutedComponents,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    RouteModule,
    HttpModule
  ],
  providers: [
    RegionService,
    UsersService,
    FactsheetsService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
