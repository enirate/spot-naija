import { SpotNaijaPage } from './app.po';

describe('spot-naija App', function() {
  let page: SpotNaijaPage;

  beforeEach(() => {
    page = new SpotNaijaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
